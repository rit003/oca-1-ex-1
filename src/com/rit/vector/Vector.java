package com.rit.vector;

public class Vector {
	double V_x ,V_y,V_z;

	public double getV_x() {
		return V_x;
	}

	public void setV_x(double v_x) {
		V_x = v_x;
	}

	public double getV_y() {
		return V_y;
	}

	public void setV_y(double v_y) {
		V_y = v_y;
	}

	public double getV_z() {
		return V_z;
	}

	public void setV_z(double v_z) {
		V_z = v_z;
	}

	public Vector(double v_x, double v_y, double v_z) {
		super();
		V_x = v_x;
		V_y = v_y;
		V_z = v_z;
	}

	
	

}
