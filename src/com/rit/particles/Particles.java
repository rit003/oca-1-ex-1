package com.rit.particles;
import java.util.*;
import com.rit.vector.Vector;
public class Particles {
	public static void main(String[] args){
	List<Vector> PositionVectors=new ArrayList<Vector>();  
	Vector b1=new Vector(0,0,0);  
	Vector b2=new Vector(5.0,0.5,1.5);  
	Vector b3=new Vector(10.5,1.4,5.5);
	PositionVectors.add(b1);
	PositionVectors.add(b2);
	PositionVectors.add(b3);
	for (Vector positionVector : PositionVectors) {
	    System.out.println(positionVector.getV_x()+ ", "+positionVector.getV_y()+","+positionVector.getV_z());
    }
   }
}
